import express from "express";

const port = 3000;

const app = express();

app.use(express.static("./public"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
// =====================================================
app.get("/", (req, res) => {
  console.log("Hello Express!");
  return res.send("Hello Express!");
});
// =====================================================
app.use((req, res, next) => {
  console.log("ERROR req.path: ", req.path);
  const err = new Error("**********PAGE NOT FOUND**********");
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  res
    .status(err.status || 500)
    .json({ status: err.status, message: err.message, stack: err.stack });
});

// =====================================================
app.listen(port, () => {
  console.log(`Server is running on port ${port}...`);
});
